Create Table If Not Exists users (
    id serial Primary Key, 
    name varchar(100),
    description varchar(100)
);
