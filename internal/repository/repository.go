package repository

import (
	"context"

	"gitlab.com/Elevarup/user/internal/models"
)

type (
	DBRepository interface {
		Close()
		List(ctx context.Context) ([]*models.User, error)
		GetByID(ctx context.Context, ID int) (*models.User, error)
		Insert(ctx context.Context, user *models.User) error
		DeleteByID(ctx context.Context, ID int) error
	}
)

var (
	db DBRepository
)

func SetRepository(r DBRepository) {
	db = r
}

func Close() {
	db.Close()
}

func List(ctx context.Context) ([]*models.User, error) {
	return db.List(ctx)
}
func GetByID(ctx context.Context, ID int) (*models.User, error) {
	return db.GetByID(ctx, ID)
}
func Insert(ctx context.Context, user *models.User) error {
	return db.Insert(ctx, user)
}
func DeleteByID(ctx context.Context, ID int) error {
	return db.DeleteByID(ctx, ID)
}
