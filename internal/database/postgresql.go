package database

import (
	"context"
	"database/sql"

	_ "github.com/lib/pq"

	"gitlab.com/Elevarup/user/internal/models"
)

type (
	PostgresRepository struct {
		db *sql.DB
	}
)

func NewPostgresRepository(url string) (*PostgresRepository, error) {
	db, err := sql.Open("postgres", url)
	if err != nil {
		return nil, err
	}
	err = db.Ping()

	return &PostgresRepository{db}, err
}

func (p *PostgresRepository) Close() {
	p.db.Close()
}
func (p *PostgresRepository) List(ctx context.Context) ([]*models.User, error) {
	rows, err := p.db.QueryContext(ctx, "Select id, name, description From users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	users := []*models.User{}

	for rows.Next() {
		user := &models.User{}
		if err := rows.Scan(
			&user.ID,
			&user.Name,
			&user.Description,
		); err != nil {
			return nil, err
		}
		users = append(users, user)
	}

	return users, nil
}
func (p *PostgresRepository) GetByID(ctx context.Context, ID int) (*models.User, error) {
	row := p.db.QueryRowContext(ctx, "Select id, name, description From users Where id = $1", ID)

	if row.Err() != nil {
		return nil, row.Err()
	}

	user := &models.User{}
	row.Scan(user.ID, user.Name, user.Description)

	return user, nil

}
func (p *PostgresRepository) Insert(ctx context.Context, user *models.User) error {
	_, err := p.db.ExecContext(ctx, "Insert Into users (name, description) Values($1,$2) ", user.Name, user.Description)
	if err != nil {
		return err
	}

	return nil
}
func (p *PostgresRepository) DeleteByID(ctx context.Context, ID int) error {
	_, err := p.db.ExecContext(ctx, "Delete From users Where id = $1", ID)
	return err
}
