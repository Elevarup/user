ARG GO_VERSION=1.21.0

# Stage 1: Build the Go application
FROM golang:${GO_VERSION}-alpine as builder

RUN go env -w GOPROXY=direct
RUN apk add --no-cache git
RUN apk --no-cache add ca-certificates && update-ca-certificates

WORKDIR /src

# Copy the vendor directory and go.mod file to the working directory
COPY go.mod go.sum ./
RUN go mod download

# Copy the rest of the application source code to the working directory
COPY . .
#COPY .env .env

# Build the Go application
# RUN go build -o app
RUN go install ./...

# Stage 2: Create the final lightweight container
FROM alpine:latest

WORKDIR /usr/bin

# Copy the built binary from the builder stage
COPY --from=builder /go/bin .
COPY .env .env
