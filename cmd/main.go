package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"gitlab.com/Elevarup/user/internal/database"
	"gitlab.com/Elevarup/user/internal/models"
	"gitlab.com/Elevarup/user/internal/repository"
)

func main() {
	err := godotenv.Load()

	user := os.Getenv("POSTGRES_USER")
	pass := os.Getenv("POSTGRES_PASSWORD")
	db := os.Getenv("POSTGRES_DB")
	if err != nil {
		log.Fatal(".env not found")
	}

	time.Sleep(5 * time.Second)
	addr := fmt.Sprintf("postgres://%s:%s@elepost/%s?sslmode=disable", user, pass, db)
	fmt.Println(addr)

	var repo repository.DBRepository
	for i := 0; i < 5; i++ {
		repo, err = database.NewPostgresRepository(addr)
		if err == nil {
			break
		}
		time.Sleep(2 * time.Second)
	}
	if err != nil {
		log.Fatal("DB: Connection not established.")
	}

	log.Println("Connection established successfully.")

	repository.SetRepository(repo)

	router := newRouter()
	if err := http.ListenAndServe(":9090", router); err != nil {
		log.Fatal(err)
	}
}

func newRouter() (router *mux.Router) {
	router = mux.NewRouter()
	router.HandleFunc("/elevar", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		log.Println("Handler /elevar")
		user := models.User{
			Name:        "elevar",
			Description: "elevar-description",
		}

		err := repository.Insert(r.Context(), &user)
		if err != nil {
			errCustom := struct {
				Description string `json:"description"`
				Code        int    `json:"code"`
			}{
				Description: err.Error(),
				Code:        http.StatusInternalServerError,
			}

			w.WriteHeader(http.StatusInternalServerError)
			json.NewEncoder(w).Encode(errCustom)
			return
		}

		okStruct := struct {
			Description string `json:"description"`
			Status      bool   `json:"status"`
		}{
			Description: "todo Good",
			Status:      true,
		}
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(okStruct)

	}).Methods(http.MethodGet)

	router.HandleFunc("/elevar/list", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")

		users, err := repository.List(r.Context())
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		okStruct := struct {
			Description string `json:"description"`
			Status      bool   `json:"status"`
			Data        any    `json:"data"`
		}{
			Description: "todo Good",
			Status:      true,
			Data:        users,
		}
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(okStruct)

	}).Methods(http.MethodGet)

	return
}
