BIN=bin/exec
MAIN=./cmd/*

all: clean build run

run: $(BIN)
	./$(BIN)
build:
	go build -o $(BIN) $(MAIN)
clean:
	rm -rf $(BIN)
